import com.google.auth.oauth2.GoogleCredentials;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.bigquery.BigQuery;
import com.google.cloud.bigquery.BigQueryOptions;
import com.google.cloud.bigquery.Table;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.stream.StreamSupport;

/**
 * @author vvserdiuk on 10.11.17.
 */
public class AppExpiration {

    private final static String PATTERN = "tmp.+";

    public static void main(String[] args) throws IOException {
        String projectId = args[0];
        String datasetId = args[1];

        BigQuery bigquery;
        if (args.length == 3) {
            String keyPath = args[2];
            bigquery = initBigQuery(projectId, keyPath);
        } else {
            bigquery = initBigQuery(projectId);
        }

        ExpirationService expirationService = new ExpirationService();
        expirationService.setExpiration(bigquery, datasetId, 2, ChronoUnit.WEEKS, PATTERN);
    }


    /**
     * Init BigQuery by its projectId. Uses default credentials from environment variable
     */
    private static BigQuery initBigQuery(String projectId) throws IOException {
        GoogleCredentials credentials = GoogleCredentials.getApplicationDefault();

        return BigQueryOptions.newBuilder()
                .setProjectId(projectId)
                .setCredentials(credentials).build().getService();
    }

    /**
     * Init BigQuery by its projectId. Gets credentials from keyPath
     */
    private static BigQuery initBigQuery(String projectId, String keyPath) throws IOException {
        GoogleCredentials credentials;
        File credentialsPath = new File(keyPath);

        try (FileInputStream serviceAccountStream = new FileInputStream(credentialsPath)) {
            credentials = ServiceAccountCredentials.fromStream(serviceAccountStream);
        }

        return BigQueryOptions.newBuilder()
                .setProjectId(projectId)
                .setCredentials(credentials).build().getService();
    }

}
