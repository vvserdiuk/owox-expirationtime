import com.google.cloud.bigquery.BigQuery;
import com.google.cloud.bigquery.Table;

import java.time.LocalDateTime;
import java.time.ZoneId;

import java.time.temporal.TemporalUnit;
import java.util.stream.StreamSupport;

/**
 * @author vvserdiuk on 10.11.17.
 */
public class ExpirationService {


    /**
     * Sets expiration time in given term for all tables in dataset, which matches pattern
     *
     * @param bigQuery BigQuery object with predefined credentials and projectId
     * @param amount the amount of the unit to add
     * @param temporalUnit the unit of the amount to add
     * @param datasetId dataset where to look for tables
     * @param pattern   pattern to match
     */
    public void setExpiration(BigQuery bigQuery, String datasetId, long amount, TemporalUnit temporalUnit, String pattern){
        Iterable<Table> tables = bigQuery.listTables(datasetId).getValues();
        StreamSupport.stream(tables.spliterator(), false)
                .filter(t -> t.getTableId().getTable().matches(pattern))
                .forEach(t -> {
                    Table.Builder builder = t.toBuilder()
                            .setExpirationTime(LocalDateTime.now().plus(amount, temporalUnit)
                                    .atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());

                    t = builder.build();
                    bigQuery.update(t);
                });
    }
}
