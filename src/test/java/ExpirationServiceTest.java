import com.google.auth.oauth2.GoogleCredentials;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.bigquery.*;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.stream.StreamSupport;

import static org.junit.Assert.*;

/**
 * @author vvserdiuk on 10.11.17.
 */
public class ExpirationServiceTest {

    private final static String PROJECT_ID    = "vvserdiuk-test";
    private final static String DATASET_ID    = "test";
    private final static String PATTERN = "tmp.+";

    ExpirationService expirationService = new ExpirationService();

    BigQuery bigQuery;
    StandardTableDefinition definition;
    Schema schema;


    @Before
    public void setUp() throws Exception {
        GoogleCredentials credentials;
        File credentialsPath = new File(this.getClass().getClassLoader().getResource("vvserdiuk-test-credentials.json").toURI());

        try (FileInputStream serviceAccountStream = new FileInputStream(credentialsPath)) {
            credentials = ServiceAccountCredentials.fromStream(serviceAccountStream);
        }

        bigQuery =  BigQueryOptions.newBuilder()
                .setProjectId(PROJECT_ID)
                .setCredentials(credentials).build().getService();
    }

    //tests to equal expiration date to minutes, ignore seconds and nanoseconds
    @Test
    public void setExpirationTestPrecisionToMinutes() throws Exception {
        LocalDateTime localDateTime = LocalDateTime.now().plus(2, ChronoUnit.WEEKS);
        LocalDateTime expected = localDateTime.withNano(0).withSecond(0);

        expirationService.setExpiration(bigQuery, DATASET_ID, 2, ChronoUnit.WEEKS,PATTERN);

        final Iterable<Table> tables = bigQuery.listTables(DATASET_ID).getValues();
        StreamSupport.stream(tables.spliterator(), false)
                .filter(t -> t.getTableId().getTable().matches(PATTERN))
                .forEach(t -> {
                    final long actualExpirationTime = bigQuery.getTable(t.getTableId()).getExpirationTime();
                    final LocalDateTime tmp = LocalDateTime.ofInstant(Instant.ofEpochMilli(actualExpirationTime), ZoneId.systemDefault());
                    LocalDateTime actual = tmp.withNano(0).withSecond(0);

                    assertEquals(expected, actual);
                });
    }

}